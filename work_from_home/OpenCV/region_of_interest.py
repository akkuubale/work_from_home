import cv2
import numpy as np
from datetime import datetime
from matplotlib import pyplot as plt
import pytesseract

#region of interest
def region_of_interest(img,vertices):
    mask=np.zeros_like(img)
    mask_color=255
    cv2.fillPoly(mask,vertices,mask_color)
    #cv2.imshow('image',mask)
    cropped_img=cv2.bitwise_and(img,mask)
    #cv2.imshow('image',cropped_img)
    #cv2.imshow('image',mask)
    return cropped_img

def process(img):
        #cv2.imshow('image',img)
        #img=cv2.cvtColor(img,cv2.COLOR_BGR2RGB)
        h=img.shape[0]
        w=img.shape[1]
        #print(h,w)
        roi=[
        (0,h),
        (644,533),
        (w,h)
        ]
        Gray_img=cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
        canny_img=cv2.Canny(Gray_img,50,150)
        cropped_canny_img=region_of_interest(canny_img,np.array([roi],np.int32))
        img_lines=cv2.HoughLinesP(cropped_canny_img,1,np.pi/180,60,minLineLength=10,maxLineGap=10)
        for line in img_lines:
            x1,y1,x2,y2=line[0]
            cv2.line(img,(x1,y1),(x2,y2),(0,255,0),2)
        cv2.imshow('video',img)

cap=cv2.VideoCapture('test.mp4')

while(cap.isOpened()):
    ret,frame=cap.read()
    #cv2.imshow('video1',frame)
    process(frame)
    #plt.imshow(frame)
    #plt.show()
    if(cv2.waitKey(1)==27):
        break

cap.release()

cv2.destroyAllWindows()      
