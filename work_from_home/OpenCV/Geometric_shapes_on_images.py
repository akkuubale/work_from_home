
import cv2
import numpy as np
from datetime import datetime
from matplotlib import pyplot as plt
import pytesseract

img=cv2.imread('lena.jpg',1)
#img=cv2.line(img,(0,0),(255,255),(255,7,255),5)#image,coordinate 1,coordinate 2,color(BGR),thickness
#img=cv2.arrowedine(img,(0,255),(255,255),(255,7,255),5)
#img=cv2.rectangle(img,(0,0),(255,255),(0,56,34),-1)#image,coordinate 1 left corner,coordinate 2 right corner,color,thickness(-1 for filled)
#img=cv2.circle(img,(255,255),63,(0,0,234),3)#center,radius,color,thickness
font=cv2.FONT_HERSHEY_SIMPLEX
img=cv2.putText(img,'Ubale',(0,255),font,4,(0,87,45),5,cv2.LINE_AA) #image,text,cordinate start,font,font size,color,thickeness,line type
cv2.imshow('image',img)
cv2.waitkey(0)
cv2.destroyAllWindows()
