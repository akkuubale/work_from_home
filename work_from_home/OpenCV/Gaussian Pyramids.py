import cv2
import numpy as np
from datetime import datetime
from matplotlib import pyplot as plt
import pytesseract
#Gsussian pyramid of resolutions images(original,1/2,1/4..)
img=cv2.imread('lena.jpg')
lr1=cv2.pyrDown(img)#converting to low resolution and blurr
lr2=cv2.pyrDown(lr1)
hr1=cv2.pyrUp(lr2)#converting to high resolution but blurr
cv2.imshow('original image',img)
cv2.imshow('pyrdown 1 image',lr1)
cv2.imshow('pyrdown 2 image',lr2)
cv2.imshow('pyrup 1 image',hr1)
cv2.waitKey(0)
cv2.destroyAllWindows()
