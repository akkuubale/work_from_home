import cv2
import numpy as np
from datetime import datetime
from matplotlib import pyplot as plt
import pytesseract

img=cv2.imread('shapes.png')
gray_img=cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
blur=cv2.medianBlur(gray_img,5)
cv2.imshow('j',blur)
circles=cv2.HoughCircles(blur,cv2.HOUGH_GRADIENT,1,20,param1=50,param2=30,minRadius=0,maxRadius=0)#img,method,dp,mindistancebetweencircles,param1,param2,minradius,maxradius
#print(circles)
circles=np.uint16(np.around(circles))
print(circles[0])
for (x,y,r) in circles[0,:]:
    #print(circle)
    cv2.circle(img,(x,y),r,(0,255,0),2)


cv2.imshow('image',img)
cv2.waitKey(0)
cv2.destroyAllWindows()
