import cv2
import numpy as np
from datetime import datetime
from matplotlib import pyplot as plt
import pytesseract

#events=[i for i in dir(cv2) if 'EVENT' in i]
#print(events)
def click_event(event,x,y,flags,param):
      if event==cv2.EVENT_LBUTTONDOWN:
             font=cv2.FONT_HERSHEY_SIMPLEX
             cv2.putText(img,str(x)+' '+str(y),(x,y),font,1,(0,34,54),2)
             cv2.imshow('image',img)
      if event==cv2.EVENT_RBUTTONDOWN:
             blue=img[y,x,0]
             green=img[y,x,1]
             red=img[y,x,2]
             font=cv2.FONT_HERSHEY_SIMPLEX
             cv2.putText(img,str(blue)+' '+str(green)+' '+str(red),(x,y),font,1,(0,23,23),2)
             cv2.imshow('image',img)
img=cv2.imread('lena.jpg',1)

cv2.imshow('image',img)

cv2.setMouseCallback('image',click_event)

cv2.waitKey(0)

cv2.destroyAllWindows()
