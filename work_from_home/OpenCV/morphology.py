import cv2
import numpy as np
from datetime import datetime
from matplotlib import pyplot as plt
import pytesseract

img=cv2.imread('j.jpg',0)
_,mask=cv2.threshold(img,220,255,cv2.THRESH_BINARY_INV)

kernal=np.ones((2,2),np.uint8)
dilation=cv2.dilate(mask,kernal,iterations=2)#removing black dots on mask#keral=square shape size
erosion=cv2.erode(mask,kernal,iterations=2)#boundries eroded
opening=cv2.morphologyEx(mask,cv2.MORPH_OPEN,kernal)#erosion followed by dialation
closing=cv2.morphologyEx(mask,cv2.MORPH_CLOSE,kernal)#dialation followed by erosion
#You can use other mophology such as cv2.MORPH_GRADIENT
images=[img,mask,dilation,erosion,opening,closing]
titles=['image','mask','dilation','ersion','opening','closing']
for i in range(0,len(images)):
      plt.subplot(2,3,i+1),plt.imshow(images[i],'gray')
      plt.title(titles[i])
      plt.xticks([]),plt.yticks([])
      
plt.show()
