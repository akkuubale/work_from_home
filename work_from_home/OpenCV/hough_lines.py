import cv2
import numpy as np
from datetime import datetime
from matplotlib import pyplot as plt
import pytesseract
img=cv2.imread('sudoku.png')
gray=cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
edges=cv2.Canny(gray,50,150)
lines=cv2.HoughLinesP(edges,1,np.pi/180,100,minLineLength=100,maxLineGap=10)#(img,rho,theta,threshold,,)
for line in lines:
    print(line)
    x1,y1,x2,y2=line[0]
    cv2.line(img,(x1,y1),(x2,y2),(0,255,0),2)
    
cv2.imshow('image',img)
cv2.waitKey(0)
cv2.destroyAllWindows()
