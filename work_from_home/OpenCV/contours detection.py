import cv2
import numpy as np
from datetime import datetime
from matplotlib import pyplot as plt
import pytesseract

img=cv2.imread('opencv-logo.png')
imgray=cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
ret,thresh=cv2.threshold(imgray,127,255,0)
cv2.imshow('thresh',thresh)
contours,hierarchy=cv2.findContours(thresh,cv2.RETR_TREE,cv2.CHAIN_APPROX_NONE)#(thresh,method,mode)
print(len(contours))

cv2.drawContours(img,contours,-1,(0,0,255),2)
cv2.imshow('image',img)
cv2.waitKey(0)
cv2.destroyAllWindows()
