import cv2
import numpy as np
from datetime import datetime
from matplotlib import pyplot as plt
import pytesseract

cap=cv2.VideoCapture(0)
cap.set(3,1208)#cv2.CAP_PROP_FRAME_WIDTH=3
cap.set(4,720)#cv2.CAP_PROP_FRAME_HEIGHT=4
fourcc=cv2.VideoWriter_fourcc(*'xvid')
out=cv2.VideoWriter('output.avi',fourcc,100,(640,480))#filename,fourcc,frames/sec,(width,height)
while(cap.isOpened()):
      ret,frame=cap.read()
      if(ret==True):
            #print(cap.get(cv2.CAP_PROP_FRAME_WIDTH ))#or print(cap.get(3)
            #print(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))#or print(cap.get(4)
            out.write(frame)
            #frame=cv2.cvtColor(frame,cv2.COLOR_BGR2GRAY)
            text='Width: '+str(cap.get(3))+' Height: '+str(cap.get(4))+'  '+str(datetime.now())
            font=cv2.FONT_HERSHEY_SIMPLEX                                             
            frame=cv2.putText(frame,text,(0,255),font,1,(0,34,54),5,cv2.LINE_AA)                        
            cv2.imshow('frame',frame)
            if(cv2.waitKey(1)==ord('q')):
               break
      else:
             break
cap.release()
out.release()
cv2.destroyAllWindows()
