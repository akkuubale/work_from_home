import cv2
import numpy as np
from datetime import datetime
from matplotlib import pyplot as plt
import pytesseract

img=cv2.imread('messi5.jpg')
grey_img=cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
template=cv2.imread('messi_face.jpg',0)
#methods = ['cv.TM_CCOEFF', 'cv.TM_CCOEFF_NORMED', 'cv.TM_CCORR','cv.TM_CCORR_NORMED', 'cv.TM_SQDIFF', 'cv.TM_SQDIFF_NORMED']
w,h=template.shape[::-1]
res=cv2.matchTemplate(grey_img,template,cv2.TM_CCORR_NORMED)#(img,template,method)#Every method gives different values
print(res)
threshold=0.97
loc=np.where(res>=threshold)
print(loc)
for pt in zip(*loc[::-1]):
    print(pt)
    cv2.rectangle(img,pt,(pt[0]+w,pt[1]+h),(0,0.255),2)

cv2.imshow('image',img)
cv2.waitKey(0)
cv2.destroyAllWindows()
