import cv2
import numpy as np
from datetime import datetime
from matplotlib import pyplot as plt
import pytesseract

img=cv2.imread('lena.jpg')
layer=img.copy()
gp=[layer]#gaussian pyramid
for i in range(6):
      layer=cv2.pyrDown(layer)
      gp.append(layer)
      #cv2.imshow(str(i),layer)

layer=gp[5]
cv2.imshow('upper level Gaussian pyramid',layer)
lp=[layer]#laplacian pyramid #helps to blend and recreate

for i in range(5,0,-1):
      gaussian_extend=cv2.pyrUp(gp[i])
      laplacian=cv2.subtract(gp[i-1],gaussian_extend)
      lp.append(laplacian)
      cv2.imshow(str(i),laplacian)
cv2.waitKey(0)
cv2.destroyAllWindows()
