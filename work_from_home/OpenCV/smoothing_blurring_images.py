import cv2
import numpy as np
from datetime import datetime
from matplotlib import pyplot as plt
import pytesseract

img=cv2.imread('salt_pepper.jpg')
img=cv2.cvtColor(img,cv2.COLOR_BGR2RGB)#necessary for matplot

kernal=np.ones((5,5),np.float32)/25 #matrix/kheight*kwidth

dest=cv2.filter2D(img,-1,kernal)#noises removed and blureed
blur=cv2.blur(img,(5,5))#noises removed and blureed
gblur=cv2.GaussianBlur(img,(5,5),0)#noises removed and blureed
median=cv2.medianBlur(img,5)#for salt and pepper images#img,kernal size must be odd except 1(gives original)

images=[img,dest,blur,gblur,median]
titles=['image','dest','blur','gblur','median']
for i in range(0,len(images)):
      plt.subplot(2,3,i+1),plt.imshow(images[i],'gray')
      plt.title(titles[i])
      plt.xticks([]),plt.yticks([])
      
plt.show()
