import cv2
import numpy as np
from datetime import datetime
from matplotlib import pyplot as plt
import pytesseract


def click_event(event,x,y,flags,param):
      if event==cv2.EVENT_RBUTTONDOWN:
             blue=img[y,x,0]
             green=img[y,x,1]
             red=img[y,x,2]
             cv2.circle(img,(x,y),1,(0,0,24),-1)
             colorimage=np.zeros((512,512,3),np.uint8)
             colorimage[:]=[blue,green,red]
             
             cv2.imshow('color',colorimage)
             
img=cv2.imread('lena.jpg',1)

cv2.imshow('image',img)

cv2.setMouseCallback('image',click_event)

cv2.waitKey(0)

cv2.destroyAllWindows()
