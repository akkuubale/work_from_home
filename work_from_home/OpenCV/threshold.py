
import cv2
import numpy as np
from datetime import datetime
from matplotlib import pyplot as plt
import pytesseract


img=cv2.imread('gradient.jpg',0)
cv2.imshow('image',img)
_, th1=cv2.threshold(img,180,200,cv2.THRESH_BINARY_INV)
_, th2=cv2.threshold(img,120,255,cv2.THRESH_TRUNC)
_, th3=cv2.threshold(img,120,255,cv2.THRESH_TOZERO)
_, th4=cv2.threshold(img,120,255,cv2.THRESH_TOZERO_INV)

images=[img,th1,th2,th3,th4]
titles=['original','th1','th2','th3','th4']
for i in range(0,len(images)):
      plt.subplot(2,3,i+1),plt.imshow(images[i],'gray')
      plt.title(titles[i])
      plt.xticks([]),plt.yticks([])

plt.show()
