import cv2
import numpy as np
from datetime import datetime
from matplotlib import pyplot as plt
import pytesseract

face_cascade=cv2.CascadeClassifier('haarcascade_frontalcatface_default.xml')
eye_cascade=cv2.CascadeClassifier('haarcascade_eye_tree_eyeglasses.xml')
cap=cv2.VideoCapture(0)
#cv2.imshow('image',img)
while(cap.isOpened()):
      ret,frame=cap.read()
      if(ret==True):
            img=cv2.cvtColor(frame,cv2.COLOR_BGR2GRAY)
            faces=face_cascade.detectMultiScale(img)#img,scale,minNeighbours
            #print(faces)
            for  (x,y,w,h) in faces:
                cv2.rectangle(frame,(x,y),(x+w,y+h),(0,255,0),3)
                roi_gray=img[y:y+h,x:x+w]
                roi_color=frame[y:y+h,x:x+w]
                eyes=eye_cascade.detectMultiScale(roi_gray)
                #print(eyes)
                for (ex,ey,ew,eh) in eyes:
                   cv2.rectangle(roi_color,(ex,ey),(ex+ew,ey+eh),(0,255,0),3)
            cv2.imshow('face',frame)
            if(cv2.waitKey(1)==ord('q')):
                break

cap.release()
cv2.destroyAllWindows()
