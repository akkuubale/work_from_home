import cv2
import numpy as np
from datetime import datetime
from matplotlib import pyplot as plt
import pytesseract

cap=cv2.VideoCapture(0)
while(cap.isOpened()):
      ret,frame=cap.read()
      if(ret==True):
            gray=cv2.cvtColor(frame,cv2.COLOR_BGR2GRAY)
            blur=cv2.GaussianBlur(gray,(5,5),0)
            ret,thresh=cv2.threshold(blur,200,255,cv2.THRESH_BINARY)
            dilated=cv2.dilate(thresh,None,iterations=3)
            contours,hierarchy=cv2.findContours(dilated,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
            print(len(contours))
            cropped=None
            for contour in contours:
                approx=cv2.approxPolyDP(contour,0.01*cv2.arcLength(contour,True),True)    
                if(len(approx)==4 ):
                    (x,y,w,h)=cv2.boundingRect(approx)
                    if(cv2.contourArea(contour)>100):
                          cropped=frame[y:y+h,x:x+w]
                          cv2.drawContours(frame,[approx],-1,(0,0,255),3)
                          pytesseract.pytesseract.tesseract_cmd = 'C:/Program Files (x86)/Tesseract-OCR/tesseract'
                          text = pytesseract.image_to_string(cropped)
                          print("Detected Number is:", text)
                          cv2.imshow('plate',cropped)
            cv2.imshow('frame',frame)
            if(cv2.waitKey(1)==ord('q')):
               break
      else:
             break
cap.release()
cv2.destroyAllWindows()
