import cv2
import numpy as np
from datetime import datetime
from matplotlib import pyplot as plt
import pytesseract

img1=cv2.imread('lena.jpg',-1)
img2=cv2.imread('messi5.jpg',-1)
images=[img1,img2]
titles=['lena','messi']
for i in range(0,len(images)):
      plt.subplot(2,3,i+1),plt.imshow(images[i],'gray')
      plt.title(titles[i])
      plt.xticks([]),plt.yticks([])
      
plt.show()
