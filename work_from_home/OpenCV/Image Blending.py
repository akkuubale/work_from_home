import cv2
import numpy as np
from datetime import datetime
from matplotlib import pyplot as plt
import pytesseract


orange=cv2.imread('orange.jpg')
apple=cv2.imread('apple.jpg')
apple=cv2.resize(apple,(512,512))
orange=cv2.resize(orange,(512,512))
print(apple.shape)
print(orange.shape)
apple_orange=np.hstack((apple[:, :256],orange[:,256:]))#adding images

#gaussian pyramid for apple
apple_copy=apple.copy()
gp_apple=[apple_copy]
for i in range(6):
      apple_copy=cv2.pyrDown(apple_copy)
      gp_apple.append(apple_copy)
      

      
#gaussian pyramid for orange
orange_copy=orange.copy()
gp_orange=[orange_copy]
for i in range(6):
      orange_copy=cv2.pyrDown(orange_copy)
      gp_orange.append(orange_copy)
      
#laplacian pyramid for apple
apple_copy=gp_apple[5]
lp_apple=[apple_copy]
for i in range(5,0,-1):
      gaussian_extended=cv2.pyrUp(gp_apple[i])
      #print(gp_apple[i-1].shape,gaussian_extended.shape)
      laplacian=cv2.subtract(gp_apple[i-1],gaussian_extended)
      lp_apple.append(laplacian)
      

#laplacian pyramid for orange
orange_copy=gp_orange[5]
lp_orange=[orange_copy]
for i in range(5,0,-1):
      gaussian_extended=cv2.pyrUp(gp_orange[i])
      laplacian=cv2.subtract(gp_orange[i-1],gaussian_extended)
      lp_orange.append(laplacian)


      
#adding two halves of apple and orange
apple_orange_pyramid=[]
for apple_lap,orange_lap in zip(lp_apple,lp_orange):
      cols,rows,ch=apple_lap.shape
      laplacian=np.hstack((apple_lap[:,0:int(cols/2)],orange_lap[:,int(cols/2):]))
      apple_orange_pyramid.append(laplacian)




#reconstruct
apple_orange_reconstruct=apple_orange_pyramid[0]
for i in range(1,6): 
      apple_orange_reconstruct=cv2.pyrUp(apple_orange_reconstruct)      
      #cv2.imshow(str(i),apple_orange_reconstruct)
      apple_orange_reconstruct=cv2.add(apple_orange_pyramid[i],apple_orange_reconstruct)
      cv2.imshow(str(i)+str(i),apple_orange_reconstruct)


#cv2.imshow('image',apple_orange_reconstruct)
cv2.waitKey(0)
cv2.destroyAllWindows()
