import cv2
import numpy as np
from datetime import datetime
from matplotlib import pyplot as plt
import pytesseract

def nothing(x):
      print(x)
      
img=np.zeros((512,512,3),np.uint8)
cv2.namedWindow('image')

cv2.createTrackbar('B','image',0,255,nothing)#(bar name,window name,start,end,funtion on change)
cv2.createTrackbar('G','image',0,255,nothing)
cv2.createTrackbar('R','image',0,255,nothing)
cv2.createTrackbar('on/off','image',0,1,nothing)

while(1):
      cv2.imshow('image',img)
      k=cv2.waitKey(1)
      if(k==27):
            break
      b=cv2.getTrackbarPos('B','image')
      g=cv2.getTrackbarPos('G','image')
      r=cv2.getTrackbarPos('R','image')

      switch=cv2.getTrackbarPos('on/off','image')
      if(switch==1):
        img[:]=0
      else:
        img[:]=[b,g,r]



cv2.destroyAllWindows()

