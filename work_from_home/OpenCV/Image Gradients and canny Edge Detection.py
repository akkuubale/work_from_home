import cv2
import numpy as np
from datetime import datetime
from matplotlib import pyplot as plt
import pytesseract


img=cv2.imread('messi5.jpg',0)

lap=cv2.Laplacian(img,cv2.CV_64F,ksize=3)#(image,datatype)
lap=np.uint8(np.absolute(lap))

sobelx=cv2.Sobel(img,cv2.CV_64F,1,0)#(image,datatype,dx,dy)dx=1 means sobelx and dy=1 means sobely
sobelx=np.uint8(np.absolute(sobelx))

sobely=cv2.Sobel(img,cv2.CV_64F,0,1)#intensity in x direction
sobely=np.uint8(np.absolute(sobely))#intensity in y direction

sobelcombined=cv2.bitwise_or(sobelx,sobely)

canny=cv2.Canny(img,100,200)#(image,threshold1,threshold2)##canny detection is of 5 steps(noise,gradient,non-maximum suppression,double threshold,edge tracking)


images=[img,lap,sobelx,sobely,sobelcombined,canny]
titles=['image','laplacian','sobelx','sobely','sobelcombined','canny']
for i in range(0,len(images)):
      plt.subplot(3,2,i+1),plt.imshow(images[i],'gray')
      plt.title(titles[i])
      plt.xticks([]),plt.yticks([])
      
plt.show()
